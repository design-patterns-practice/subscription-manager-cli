from typing import FrozenSet, List

from .channel_observer import IChannelObserver

Channel = str


class ChannelEventManager:
    def __init__(self) -> None:
        self.__observers: List[IChannelObserver] = []

    def add_channel_observer(
        self, *, observer: IChannelObserver
    ) -> "ChannelEventManager":
        """Adds the observer to the list of observers that should be notified.

        :returns This ChannelEventManager for a flowing interface."""
        self.__observers.append(observer)
        return self

    def notify_on_subscription(self, *, user: str, channel: str) -> None:
        """Notifies all observers listening to the specified channel that a new user has subscribed."""
        for observer in self.__observers:
            observer.user_subscribed(user=user, channel=channel)

    def notify_on_publish(
        self, *, subscribed_users: FrozenSet[str], channel: str
    ) -> None:
        """Notifies all observers listening to the specified channel that a new video was published."""
        for observer in self.__observers:
            observer.video_published(subscribed_users=subscribed_users, channel=channel)
