from typing import FrozenSet, Protocol


class IChannelObserver(Protocol):
    def user_subscribed(self, *, user: str, channel: str) -> None:
        """Notifies this channel observer tha a new user has subscribed to a channel.

        :param user The user that has just subscribed.
        :param channel The channel that the user has subscribed to."""

    def video_published(
        self, *, subscribed_users: FrozenSet[str], channel: str
    ) -> None:
        """Notifies this channel observer that a new video was published to a channel.

        :param subscribed_users The set of users that were subscribed to this channel at the time of publishing.
        :param channel The channel that the video was published to."""
