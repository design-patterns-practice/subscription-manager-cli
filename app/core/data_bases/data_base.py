from typing import FrozenSet, Protocol


class IDataBase(Protocol):
    def add_channel(self, channel: str) -> None:
        """Adds the specified channel to the data base."""

    def has_channel(self, channel: str) -> bool:
        """Returns true if this data base contains the specified channel, false otherwise."""

    def add_subscriber(self, *, user: str, channel: str) -> None:
        """Subscribes the user to the specified channel."""

    def get_subscribers(self, *, channel: str) -> FrozenSet[str]:
        """Returns a set of all users subscribed to the specified channel."""

    def is_subscriber(self, *, user: str, channel: str) -> bool:
        """Returns true if the user is a subscriber of the specified channel, false otherwise."""
