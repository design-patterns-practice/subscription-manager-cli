from typing import Optional

from core import ChannelEventManager, IDataBase


class SubscriptionManager:
    def __init__(
        self,
        *,
        data_base: IDataBase,
        event_manager: Optional[ChannelEventManager] = None,
    ) -> None:
        self.__data_base = data_base
        self.__event_manager = event_manager

    def create_channel(self, channel: str) -> bool:
        """Creates a new channel with the specified name.

        :returns True if the specified channel did not exist and was just created, false otherwise."""
        if self.__data_base.has_channel(channel):
            return False
        else:
            self.__data_base.add_channel(channel)
            return True

    def subscribe_user(self, *, user: str, channel: str) -> bool:
        """Subscribes the given user to the specified channel.

        :returns True if the specified user was not subscribed to this channel, false otherwise."""
        if not self.__data_base.has_channel(channel):
            raise ValueError(f"The channel with the name '{channel}' does not exist.")

        if self.__data_base.is_subscriber(user=user, channel=channel):
            return False
        else:
            self.__data_base.add_subscriber(user=user, channel=channel)

            if self.__event_manager is not None:
                self.__event_manager.notify_on_subscription(user=user, channel=channel)

            return True

    def publish_video(self, *, channel: str) -> None:
        """Publishes a video to the specified channel."""
        if not self.__data_base.has_channel(channel):
            raise ValueError(f"The channel with the name '{channel}' does not exist.")

        if self.__event_manager is not None:
            subscribed_users = self.__data_base.get_subscribers(channel=channel)

            self.__event_manager.notify_on_publish(
                subscribed_users=subscribed_users, channel=channel
            )
