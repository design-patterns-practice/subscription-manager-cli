import re
from typing import Final

from core import SubscriptionManager


class CLI:
    CMD_RE_SUBSCRIBE: Final = re.compile(
        r"subscribe\s+<(?P<user>[^<>]+)>\s+to\s+<(?P<channel>[^<>]+)>"
    )
    CMD_RE_PUBLISH: Final = re.compile(r"publish\s+video\s+on\s+<(?P<channel>[^<>]+)>")

    def __init__(self, *, subscription_manager: SubscriptionManager) -> None:
        self.__subscription_manager = subscription_manager

    def start(self) -> None:
        """Launches the CLI."""
        while True:
            user_input = input(">>> ")

            if user_input == "":
                break
            elif user_input == "help":
                print(self.help_message)
                continue

            if not self.execute(user_input):
                print(f"Error: Could not execute command '{user_input}'")
                print(self.help_message)

    def execute(self, command: str) -> bool:
        """Executes the specified string command.

        :returns True if the command was successfully executed, false otherwise."""
        match = CLI.CMD_RE_SUBSCRIBE.fullmatch(command)
        if match:
            self.__process_subscribe_command(*match.group("user", "channel"))
            return True

        match = CLI.CMD_RE_PUBLISH.fullmatch(command)
        if match:
            self.__process_publish_command(match.group("channel"))
            return True

        return False

    @property
    def help_message(self) -> str:
        """Returns the help message for this Command-Line Interface."""
        return (
            "Available Commands:\n"
            "\tsubscribe <user> to <channel> - Subscribes a user to a channel.\n"
            "\tpublish video on <channel> - Publishes a video to a channel.\n"
        )

    def __process_subscribe_command(self, user: str, channel: str) -> None:
        """Subscribes the user to the specified channel."""
        self.__subscription_manager.create_channel(channel)
        self.__subscription_manager.subscribe_user(user=user, channel=channel)

    def __process_publish_command(self, channel: str) -> None:
        """Publishes a video to the specified channel."""
        self.__subscription_manager.create_channel(channel)
        self.__subscription_manager.publish_video(channel=channel)
