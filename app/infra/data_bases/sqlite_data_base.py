import sqlite3
from typing import Any, FrozenSet


class SQLiteDataBase:
    def __init__(
        self,
        *,
        db_file: str = "subscriptions.sqlite",
        init_file: str = "subscriptions.sql",
    ) -> None:
        self.__get_data_base_connection(db_file)
        self.__initialize_data_base(init_file)

    def __enter__(self) -> "SQLiteDataBase":
        return self

    def __exit__(self, exc_type: Any, exc_value: Any, exc_traceback: Any) -> None:
        self.close()

    def add_channel(self, channel: str) -> None:
        """Adds the specified channel to the data base."""
        self.__connection.cursor().execute(
            "INSERT INTO channel(name) VALUES (:channel)", {"channel": channel}
        )
        self.__connection.commit()

    def has_channel(self, channel: str) -> bool:
        """Returns true if this data base contains the specified channel, false otherwise."""
        result = self.__connection.cursor().execute(
            "SELECT COUNT(*) FROM channel WHERE name=:channel", {"channel": channel}
        )
        return int(result.fetchone()[0]) > 0

    def add_subscriber(self, *, user: str, channel: str) -> None:
        """Subscribes the user to the specified channel."""
        self.__connection.cursor().execute(
            "INSERT INTO channel_subscription(channel_id, user) VALUES (:channel_id, :user)",
            {"channel_id": self.__get_channel_id(channel), "user": user},
        )
        self.__connection.commit()

    def get_subscribers(self, *, channel: str) -> FrozenSet[str]:
        """Returns a set of all users subscribed to the specified channel."""
        return frozenset(
            str(row[0])
            for row in self.__connection.cursor().execute(
                "SELECT user FROM channel_subscription WHERE channel_id=:channel_id",
                {"channel_id": self.__get_channel_id(channel)},
            )
        )

    def is_subscriber(self, *, user: str, channel: str) -> bool:
        """Returns true if the user is a subscriber of the specified channel, false otherwise."""
        return user in self.get_subscribers(channel=channel)

    def close(self) -> None:
        """Closes all connections to the data base."""
        self.__connection.close()

    def __get_data_base_connection(self, db_file: str) -> None:
        """Creates a single data base connection for later use."""
        self.__connection = sqlite3.connect(db_file)

    def __initialize_data_base(self, init_file: str) -> None:
        """Initializes the data base according to the specified initialization file."""
        sql_script: str

        with open(file=init_file, mode="r") as sql_file:
            sql_script = sql_file.read()

        self.__connection.cursor().executescript(sql_script)

    def __get_channel_id(self, channel: str) -> int:
        """Returns the ID of the specified channel."""
        return int(
            self.__connection.cursor()
            .execute(
                "SELECT ROWID FROM channel WHERE name=:channel", {"channel": channel}
            )
            .fetchone()[0]
        )
