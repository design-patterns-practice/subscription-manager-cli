from typing import Dict, FrozenSet, Set

User = str
Channel = str


class MemoryDataBase:
    def __init__(self) -> None:
        self.__channel_subscriptions: Dict[Channel, Set[User]] = {}

    def add_channel(self, channel: str) -> None:
        """Adds the specified channel to the data base."""
        self.__channel_subscriptions[channel] = set()

    def has_channel(self, channel: str) -> bool:
        """Returns true if this data base contains the specified channel, false otherwise."""
        return channel in self.__channel_subscriptions

    def add_subscriber(self, *, user: str, channel: str) -> None:
        """Subscribes the user to the specified channel."""
        self.__channel_subscriptions[channel].add(user)

    def get_subscribers(self, *, channel: str) -> FrozenSet[str]:
        """Returns a set of all users subscribed to the specified channel."""
        return frozenset(self.__channel_subscriptions[channel])

    def is_subscriber(self, *, user: str, channel: str) -> bool:
        """Returns true if the user is a subscriber of the specified channel, false otherwise."""
        return user in self.__channel_subscriptions[channel]
