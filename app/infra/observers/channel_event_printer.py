from typing import FrozenSet


class ChannelEventPrinter:
    @staticmethod
    def user_subscribed(*, user: str, channel: str) -> None:
        """Notifies this channel observer tha a new user has subscribed to a channel.

        :param user The user that has just subscribed.
        :param channel The channel that the user has subscribed to."""

        print(f"{user} subscribed to {channel}")

    @staticmethod
    def video_published(*, subscribed_users: FrozenSet[str], channel: str) -> None:
        """Notifies this channel observer that a new video was published to a channel.

        :param subscribed_users The set of users that were subscribed to this channel at the time of publishing.
        :param channel The channel that the video was published to."""
        subscribed_users_string = "\n\t".join(subscribed_users)
        print(f"Notifying subscribers of {channel}:\n\t{subscribed_users_string}")
