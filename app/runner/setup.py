from typing import Callable

from core import ChannelEventManager, SubscriptionManager
from infra import CLI, ChannelEventPrinter, SQLiteDataBase

Application = Callable[[], None]


def setup() -> Application:
    """Sets up and returns the application to be run."""

    def app() -> None:
        with SQLiteDataBase() as data_base:
            event_manager = ChannelEventManager().add_channel_observer(
                observer=ChannelEventPrinter()
            )

            subscription_manager = SubscriptionManager(
                data_base=data_base, event_manager=event_manager
            )

            CLI(subscription_manager=subscription_manager).start()

    return app
