CREATE TABLE IF NOT EXISTS channel (
    name        VARCHAR(64)     NOT NULL
);

CREATE TABLE IF NOT EXISTS channel_subscription (
    channel_id  INTEGER         NOT NULL,
    user        VARCHAR(64)     NOT NULL,

    FOREIGN KEY(channel_id) REFERENCES channel(ROWID)
);