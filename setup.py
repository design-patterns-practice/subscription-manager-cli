from setuptools import setup

setup(
    name="Subscription Manager CLI",
    version="1.0.0",
    description="A simple REPL application.",
    package_dir={"": "app"},
    author="Zurab Mujirishvili",
    author_email="zmuji18@freeuni.edu.ge",
)
