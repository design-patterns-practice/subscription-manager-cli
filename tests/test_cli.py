"""
Test List for CLI:
1) Should parse subscribe commands correctly. Format: subscribe <username> to <channel> V
2) Should parse publish commands correctly. Format: publish video on <channel> V
3) Should handle invalid inputs. V
"""
import string
from random import choice, randint

import pytest
from core import ChannelEventManager, SubscriptionManager
from infra import CLI, MemoryDataBase
from stubs import StubChannelObserver


@pytest.fixture
def event_manager() -> ChannelEventManager:
    return ChannelEventManager()


@pytest.fixture
def subscription_manager(event_manager: ChannelEventManager) -> SubscriptionManager:
    return SubscriptionManager(data_base=MemoryDataBase(), event_manager=event_manager)


def random_string(length: int = 10) -> str:
    """Returns a random string of the specified length."""
    return "".join(choice(string.ascii_letters) for _ in range(length))


def test_should_parse_subscribe_command(
    subscription_manager: SubscriptionManager,
    event_manager: ChannelEventManager,
) -> None:
    channel = "Channel 1"
    user = "User 1"

    observer = StubChannelObserver()
    event_manager.add_channel_observer(observer=observer)

    cli = CLI(subscription_manager=subscription_manager)

    assert cli.execute(f"subscribe <{user}> to <{channel}>") is True
    assert observer.subscribed_users == {channel: [user]}


def test_should_parse_multiple_subscribe_commands(
    subscription_manager: SubscriptionManager,
    event_manager: ChannelEventManager,
) -> None:
    channel = "Test Channel"
    users = tuple(random_string() for _ in range(10))

    observer = StubChannelObserver()
    event_manager.add_channel_observer(observer=observer)

    cli = CLI(subscription_manager=subscription_manager)

    for user in users:
        white_space = randint(1, 10) * " "
        assert (
            cli.execute(
                f"subscribe{white_space}<{user}>{white_space}to{white_space}<{channel}>"
            )
            is True
        )

    assert observer.subscribed_users == {channel: list(users)}


def test_should_parse_publish_command(
    subscription_manager: SubscriptionManager,
    event_manager: ChannelEventManager,
) -> None:
    channel = "Another Channel"
    users = tuple(random_string() for _ in range(10))
    white_space = randint(1, 10) * " "

    observer = StubChannelObserver()
    event_manager.add_channel_observer(observer=observer)

    cli = CLI(subscription_manager=subscription_manager)

    for user in users:
        cli.execute(f"subscribe <{user}> to <{channel}>")

    assert (
        cli.execute(f"publish{white_space}video{white_space}on{white_space}<{channel}>")
        is True
    )
    assert observer.last_notified_users == {channel: frozenset(users)}


def test_should_handle_invalid_input(
    subscription_manager: SubscriptionManager,
    event_manager: ChannelEventManager,
) -> None:
    cli = CLI(subscription_manager=subscription_manager)
    assert cli.execute("Invalid subscribe <A> to <B>") is False
