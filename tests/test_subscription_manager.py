"""
Test List for Subscription Manager:
1) Should create a new channel. V
2) Should not create a channel if one already exists. V
1) Should subscribe a user to a channel. V
3) Should subscribe multiple users to the same channel. V
4) Should subscribe the same user to multiple channels. V
5) Should notify observer when a new user subscribes to a channel. V
6) Should notify observer when a new video is published to a channel. V
7) Should store data persistently. V
"""
import pytest
from core import ChannelEventManager, SubscriptionManager
from infra import MemoryDataBase
from stubs import StubChannelObserver


@pytest.fixture
def memory_data_base() -> MemoryDataBase:
    return MemoryDataBase()


@pytest.fixture
def event_manager() -> ChannelEventManager:
    return ChannelEventManager()


@pytest.fixture
def subscription_manager(
    memory_data_base: MemoryDataBase, event_manager: ChannelEventManager
) -> SubscriptionManager:
    return SubscriptionManager(data_base=memory_data_base, event_manager=event_manager)


def test_should_create_subscription_manager(
    subscription_manager: SubscriptionManager,
) -> None:
    assert subscription_manager is not None


def test_should_create_new_channel(subscription_manager: SubscriptionManager) -> None:
    assert subscription_manager.create_channel("Test Channel") is True


def test_should_create_multiple_channels(
    subscription_manager: SubscriptionManager,
) -> None:
    assert subscription_manager.create_channel("Test Channel 1") is True
    assert subscription_manager.create_channel("Test Channel 2") is True
    assert subscription_manager.create_channel("Test Channel 3") is True


def test_should_not_recreate_existing_channel(
    subscription_manager: SubscriptionManager,
) -> None:
    subscription_manager.create_channel("Test Channel 1")
    subscription_manager.create_channel("Test Channel 2")

    assert subscription_manager.create_channel("Test Channel 1") is False
    assert subscription_manager.create_channel("Test Channel 2") is False


def test_should_store_channels_persistently(
    subscription_manager: SubscriptionManager, memory_data_base: MemoryDataBase
) -> None:
    subscription_manager.create_channel("Test Channel 1")
    subscription_manager.create_channel("Test Channel 2")

    new_subscription_manager = SubscriptionManager(data_base=memory_data_base)

    assert new_subscription_manager.create_channel("Test Channel 1") is False
    assert new_subscription_manager.create_channel("Test Channel 2") is False


def test_should_subscribe_user_to_channel(
    subscription_manager: SubscriptionManager,
) -> None:
    subscription_manager.create_channel("New Channel")

    assert (
        subscription_manager.subscribe_user(user="User 1", channel="New Channel")
        is True
    )


def test_should_subscribe_multiple_users_to_the_same_channel(
    subscription_manager: SubscriptionManager,
) -> None:
    subscription_manager.create_channel("Another Channel")

    assert (
        subscription_manager.subscribe_user(user="User 1", channel="Another Channel")
        is True
    )
    assert (
        subscription_manager.subscribe_user(user="User 2", channel="Another Channel")
        is True
    )
    assert (
        subscription_manager.subscribe_user(user="User 3", channel="Another Channel")
        is True
    )


def test_should_not_subscribe_the_same_user_twice(
    subscription_manager: SubscriptionManager,
) -> None:
    subscription_manager.create_channel("Another Channel")
    subscription_manager.subscribe_user(user="User 1", channel="Another Channel")
    subscription_manager.subscribe_user(user="User 2", channel="Another Channel")

    assert (
        subscription_manager.subscribe_user(user="User 1", channel="Another Channel")
        is False
    )
    assert (
        subscription_manager.subscribe_user(user="User 2", channel="Another Channel")
        is False
    )


def test_should_store_subscriptions_persistently(
    subscription_manager: SubscriptionManager, memory_data_base: MemoryDataBase
) -> None:
    subscription_manager.create_channel("Test Channel 1")
    subscription_manager.create_channel("Test Channel 2")

    subscription_manager.subscribe_user(user="User 1", channel="Test Channel 1")
    subscription_manager.subscribe_user(user="User 2", channel="Test Channel 2")

    new_subscription_manager = SubscriptionManager(data_base=memory_data_base)

    assert (
        new_subscription_manager.subscribe_user(user="User 1", channel="Test Channel 1")
        is False
    )
    assert (
        new_subscription_manager.subscribe_user(user="User 2", channel="Test Channel 2")
        is False
    )

    assert (
        new_subscription_manager.subscribe_user(user="User 3", channel="Test Channel 1")
        is True
    )
    assert (
        new_subscription_manager.subscribe_user(user="User 4", channel="Test Channel 2")
        is True
    )


def test_should_subscribe_same_user_to_multiple_channels(
    subscription_manager: SubscriptionManager,
) -> None:
    subscription_manager.create_channel("Channel 1")
    subscription_manager.create_channel("Channel 2")

    assert (
        subscription_manager.subscribe_user(user="User 1", channel="Channel 1") is True
    )
    assert (
        subscription_manager.subscribe_user(user="User 1", channel="Channel 2") is True
    )


def test_should_notify_channel_observer_about_new_subscription(
    subscription_manager: SubscriptionManager, event_manager: ChannelEventManager
) -> None:
    observer = StubChannelObserver()
    event_manager.add_channel_observer(observer=observer)

    subscription_manager.create_channel(channel="Channel 1")

    subscription_manager.subscribe_user(user="User 1", channel="Channel 1")
    subscription_manager.subscribe_user(user="User 2", channel="Channel 1")
    subscription_manager.subscribe_user(user="User 3", channel="Channel 1")

    assert observer.subscribed_users == {
        "Channel 1": ["User 1", "User 2", "User 3"],
    }


def test_should_notify_channel_observer_about_subscriptions_to_multiple_channels(
    subscription_manager: SubscriptionManager, event_manager: ChannelEventManager
) -> None:
    observer = StubChannelObserver()
    event_manager.add_channel_observer(observer=observer)

    subscription_manager.create_channel(channel="A Channel")
    subscription_manager.create_channel(channel="Another Channel")

    subscription_manager.subscribe_user(user="User 1", channel="A Channel")
    subscription_manager.subscribe_user(user="User 2", channel="Another Channel")
    subscription_manager.subscribe_user(user="User 3", channel="Another Channel")

    assert observer.subscribed_users == {
        "A Channel": ["User 1"],
        "Another Channel": ["User 2", "User 3"],
    }


def test_should_publish_video_to_channel(
    subscription_manager: SubscriptionManager, event_manager: ChannelEventManager
) -> None:
    observer = StubChannelObserver()
    event_manager.add_channel_observer(observer=observer)

    subscription_manager.create_channel(channel="A Channel")
    subscription_manager.create_channel(channel="Another Channel")

    subscription_manager.subscribe_user(user="User 1", channel="A Channel")
    subscription_manager.subscribe_user(user="User 2", channel="A Channel")
    subscription_manager.subscribe_user(user="User 3", channel="A Channel")
    subscription_manager.subscribe_user(user="Alt user 1", channel="Another Channel")
    subscription_manager.subscribe_user(user="Alt user 2", channel="Another Channel")

    subscription_manager.publish_video(channel="A Channel")

    assert observer.last_notified_users == {
        "A Channel": frozenset({"User 1", "User 2", "User 3"}),
    }

    subscription_manager.subscribe_user(user="User 4", channel="A Channel")
    subscription_manager.publish_video(channel="A Channel")

    assert observer.last_notified_users == {
        "A Channel": frozenset({"User 1", "User 2", "User 3", "User 4"}),
    }
