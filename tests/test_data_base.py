"""
Test List for Data Bases:
1) Should add new channels to data base. V
2) Should check for existing channels. V
3) Should add subscribers for channels. V
4) Should return subscribers for existing channels. V
5) Should check for existing subscribers to channels. V

NOTE: These tests were written for the SQLite Data Base, however, I figured I could use them to test the in-memory
data base as well.
"""
from typing import Iterator

from core import IDataBase
from infra import MemoryDataBase, SQLiteDataBase
from pytest_cases import fixture, parametrize


@fixture  # type: ignore
def get_memory_data_base() -> MemoryDataBase:
    """Returns an in-memory data base."""
    return MemoryDataBase()


@fixture  # type: ignore
def get_sqlite_data_base() -> Iterator[SQLiteDataBase]:
    """Returns an SQLite data base."""
    data_base = SQLiteDataBase(db_file=":memory:")
    yield data_base
    data_base.close()


@parametrize("data_base", (get_memory_data_base, get_sqlite_data_base))  # type: ignore
def test_should_add_new_channels(data_base: IDataBase) -> None:
    data_base.add_channel("Channel 1")
    data_base.add_channel("Channel 2")
    data_base.add_channel("Channel 3")


@parametrize("data_base", (get_memory_data_base, get_sqlite_data_base))  # type: ignore
def test_should_find_existing_channels(data_base: IDataBase) -> None:
    assert data_base.has_channel("Channel 1") is False
    assert data_base.has_channel("Channel 2") is False
    assert data_base.has_channel("Channel 3") is False

    data_base.add_channel("Channel 1")
    data_base.add_channel("Channel 2")
    data_base.add_channel("Channel 3")

    assert data_base.has_channel("Channel 1") is True
    assert data_base.has_channel("Channel 2") is True
    assert data_base.has_channel("Channel 3") is True
    assert data_base.has_channel("Different Channel 1") is False
    assert data_base.has_channel("Different Channel 2") is False
    assert data_base.has_channel("Different Channel 3") is False


@parametrize("data_base", (get_memory_data_base, get_sqlite_data_base))  # type: ignore
def test_should_add_subscribers_to_channels(data_base: IDataBase) -> None:
    data_base.add_channel("Channel 1")
    data_base.add_channel("Channel 2")

    data_base.add_subscriber(user="User 1", channel="Channel 1")
    data_base.add_subscriber(user="User 2", channel="Channel 1")
    data_base.add_subscriber(user="User 1", channel="Channel 2")


@parametrize("data_base", (get_memory_data_base, get_sqlite_data_base))  # type: ignore
def test_should_return_all_subscribers_for_channel(data_base: IDataBase) -> None:
    data_base.add_channel("Channel 1")
    data_base.add_channel("Channel 2")

    data_base.add_subscriber(user="User 1", channel="Channel 1")
    data_base.add_subscriber(user="User 2", channel="Channel 1")
    data_base.add_subscriber(user="User 1", channel="Channel 2")

    assert data_base.get_subscribers(channel="Channel 1") == {"User 1", "User 2"}
    assert data_base.get_subscribers(channel="Channel 2") == {"User 1"}


@parametrize("data_base", (get_memory_data_base, get_sqlite_data_base))  # type: ignore
def test_should_find_existing_subscribers(data_base: IDataBase) -> None:
    data_base.add_channel("Channel 1")
    data_base.add_channel("Channel 2")

    assert data_base.is_subscriber(user="User 1", channel="Channel 1") is False
    assert data_base.is_subscriber(user="User 1", channel="Channel 2") is False

    data_base.add_subscriber(user="User 1", channel="Channel 1")

    assert data_base.is_subscriber(user="User 1", channel="Channel 1") is True
    assert data_base.is_subscriber(user="User 1", channel="Channel 2") is False

    data_base.add_subscriber(user="User 2", channel="Channel 2")

    assert data_base.is_subscriber(user="User 2", channel="Channel 1") is False
    assert data_base.is_subscriber(user="User 2", channel="Channel 2") is True
